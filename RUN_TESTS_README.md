# Different tests to check options for the pipeline
## Test inputs
### Just local fastqs
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1' --remove_recombination --tree -resume
### Just accessions
nextflow run snp_phylogeny.nf --accession_number_file test_input/accessions.txt --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1' --remove_recombination --tree -resume
### Local fastqs and accessions
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --accession_number_file test_input/accessions.txt --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'  --remove_recombination --tree -resume
### bcfs
nextflow run snp_phylogeny.nf  --bcfs 'test_input/bcfs/*.bcf'  --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'   --remove_recombination --tree -resume
### filtered bcfs
nextflow run snp_phylogeny.nf  --filtered_bcfs 'test_input/filtered_bcfs/*.bcf'  --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'   --remove_recombination --tree -resume
### filtered pseudogenomes
nextflow run snp_phylogeny.nf  --pseudogenomes 'test_input/pseudogenomes/ERR*.fas'  --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'   --remove_recombination --tree -resume
### pseudogenome alignment
nextflow run snp_phylogeny.nf  --pseudogenome_alignment 'test_input/pseudogenomes/aligned_pseudogenome.fas'  --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'   --remove_recombination --tree -resume
## Test tree options
### auto should be fasttree
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1' --remove_recombination --tree auto --alignment_size_cutoff 60 -resume
### auto should be iqtree
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1' --remove_recombination --tree auto --alignment_size_cutoff 70 -resume
### fasttree
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1' --remove_recombination --tree fasttree -resume
### iqtree
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1' --remove_recombination --tree iqtree -resume
### njtree
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1' --remove_recombination --tree nj -resume
## Test termination points
### Local fastqs and accessions terminating at bcfs
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --accession_number_file test_input/accessions.txt --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'  --termination_point bcfs --remove_recombination --tree -resume -ansi
### Local fastqs and accessions terminating at filtered_bcfs
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --accession_number_file test_input/accessions.txt --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'  --termination_point filtered_bcfs --remove_recombination --tree -resume -ansi
### Local fastqs and accessions terminating at pseudogenomes
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --accession_number_file test_input/accessions.txt --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'  --termination_point pseudogenomes --remove_recombination --tree -resume -ansi
### Local fastqs and accessions terminating at pseudogenome alignment
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --accession_number_file test_input/accessions.txt --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'  --termination_point pseudogenome_alignment --remove_recombination --tree -resume -ansi
## Test output modes
### Simple output
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1' --remove_recombination --tree --simple_output -resume
### Full output
nextflow run snp_phylogeny.nf  --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --output_dir test_output --reference test_input/NCTC13799.fna --filtering_conditions '%QUAL<25 || FORMAT/DP<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1' --remove_recombination --tree --full_output -resume
