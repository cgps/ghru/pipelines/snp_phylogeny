#!/usr/bin/env python3
import argparse
import os, sys
from Bio import AlignIO

class ParserWithErrors(argparse.ArgumentParser):
    def error(self, message):
        print('{0}\n\n'.format(message))
        self.print_help()
        sys.exit(2)

    def is_valid_file(self, parser, arg):
        if not os.path.isfile(arg):
            parser.error("The file %s does not exist!" % arg)
        else:
            return arg


def argparser():
    description = """
    A script to parse a filtered VCF and 
    """
    parser = ParserWithErrors(description = description)
    parser.add_argument("-a", "--alignment_file", required=True, 
                        help="multiple fasta alignment file path",
                        type=lambda x: parser.is_valid_file(parser, x))

    return parser

def get_alignment_size(filepath):
    alignment = AlignIO.read(filepath, "fasta")
    num_sequences = len(alignment)
    length_alignment = alignment.get_alignment_length()
    print(num_sequences, length_alignment)

if __name__ == '__main__':
    parser = argparser()
    args = parser.parse_args()
    get_alignment_size(args.alignment_file)
