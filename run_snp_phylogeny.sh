usage()
{
  echo "usage: run_snp_phylogeny -p <PARENT DIR> -r <REF GENOME> [ -fq <FASTQ PATTERN> ] [ -f <FILTERING CONDITIONS> ] [ -rr ] [ -o <OUTPUT DIR> ] [ -t ]"
  echo "  -p <PARENT DIR>: e.g /path/to/project or if using AWS S3 my-bucket/project/"
  echo "  -r <REF GENOME> : path to ref fasta file"
  echo "  -f <FILTERING CONDITIONS> (optional): default %QUAL<25 || FORMAT/DP<10 || MAX(FORMAT/ADF)<5 || MAX(FORMAT/ADR)<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1"
  echo "  -rr : remove recombination(optional)"
  echo "  -fq <FASTQ PATTERN> (optional): default '*{R,_}{1,2}*.fastq.gz'"
  echo "  -o <OUTPUT DIR> (optional): will be created as a subdir in the PARENT DIR - default snp_phylogeny_output"
  echo "  -t (optional): create tree with IQTREE"
  echo "  -s (optional): running on AWS batch - inputs are on S3 storage"
  echo "  -q (optional unless using AWS): when running on AWS batch specify the batch queue name"
  echo "  -w (optional): path to workflow (nf) file. If not specified then it will be assumed it's in the current directory"
}

while [ "$1" != "" ]; do
  case $1 in
    -a | --accessions )           shift
                                  accessions=$1
                                  ;;
    -p | --parent_directory )     shift
                                  parent_directory=$1
                                  ;;
    -r | --reference_genome )     shift
                                  reference_genome=$1
                                  ;;
    -fq | --fastq_pattern )       shift
                                  fastq_pattern=$1
                                  ;;
    -f | --filtering_conditions ) shift
                                  filtering_conditions=$1
                                  ;;
    -o | --output_dir )           shift
                                  output_dir=$1
                                  ;;
    -w | --workflow_file_dir )    shift
                                  workflow_file_dir=$1
                                  ;;
    -rr | --remove_recombination ) remove_recombination='true'
                                  ;;
    -s | --s3 )                   s3='true'
                                  ;;
    -q | --aws_batch_queue )      shift
                                  aws_batch_queue=$1
                                  ;;
    -t | --tree )                 tree='true'
                                  ;;
    -d | --debug )                debug='true'
                                  ;;                                  
    -h | --help )                 usage
                                  exit
                                  ;;

  esac
  shift
done


if [[ -z $parent_directory ]] && [[ -z $accessions ]]; then
  echo "To specify input reads you must specifiy either a parent directory with -p (reads found within a subdir fastqs)"
  echo "or a list of accessions with -a"
  usage
  exit 1
fi

if [[ -z $parent_directory ]] && [[ ! -z $accessions ]]; then
  echo "Even if specifying accessions, you must specifiy a parent directory with -p to write the outputs to"
  usage
  exit 1
fi

if [[ -z $parent_directory ]]; then
  echo "You must specifiy a parent directory with -p"
  usage
  exit 1
fi

if [[ -z $reference_genome ]]; then
  echo "You must specify the path to a FASTA reference sequence with  -r"
  usage
  exit 1
fi 
if [[ -z $output_dir ]]; then
  output_dir='snp_phylogeny_output'
fi

if [[ -z $fastq_pattern ]]; then
  fastq_pattern='*{R,_}{1,2}*.fastq.gz'
fi

if [[ -z $workflow_file_dir ]] ; then
  workflow_file_dir=$PWD
fi

if [[ ! -z $s3 ]] && [[ -z $aws_batch_queue ]]; then
  echo "I running on AWS batch you must specify an AWS Batch queue with -q"
  usage
  exit 1
fi

base_cmd="nextflow run $workflow_file_dir/snp_phylogeny.nf"

# set up output dir
if [[ -z $s3 ]]; then
  output_dir_argument="--output_dir ${parent_directory}/${output_dir}"
else
  output_dir_argument="--output_dir s3://${parent_directory}/${output_dir}"
fi

# set up work dir
if [[ -z $s3 ]]; then
  work_dir_argument="-w ${parent_directory}/work"
else
  work_dir_argument="-w s3://${parent_directory}/work"
fi

# set up reference sequence, adapter file and depth cutoff
reference_argument="--reference ${reference_genome}"
adapter_file_argument="--adapter_file $workflow_file_dir/adapters.fas"
depth_cutoff_argument="--depth_cutoff 100"
resume_argument="-resume"
ansi_argument="-ansi"

# determine profile
if [[ -z $s3 ]]; then
  profile='standard'
else
  profile='aws_batch'
fi

profile_argument="-profile $profile"

if [[ -z $accessions ]]; then
  # set up input dir
  if [[ -z $s3 ]]; then
    parent_directory_argument="--parent_directory ${parent_directory}/fastqs"
  else
    parent_directory_argument="--parent_directory s3://${parent_directory}/fastqs"
  fi
  fastq_pattern_argument="--fastq_pattern '${fastq_pattern}'"
  full_cmd="$base_cmd $parent_directory_argument $fastq_pattern_argument $reference_argument $output_dir_argument \
  $adapter_file_argument $depth_cutoff_argument $tree_argument $resume_argument $work_dir_argument $ansi_argument $profile_argument"
else
  accessions_argument="--accession_number_file ${accessions}"
  full_cmd="$base_cmd $accessions_argument $reference_argument $output_dir_argument \
  $adapter_file_argument $depth_cutoff_argument $tree_argument $resume_argument $work_dir_argument $ansi_argument $profile_argument"
fi

# Determine whether to apply non default filtering, run recombination removal and build a tree
if [[ ! -z ${filtering_conditions} ]]; then
  full_cmd="$full_cmd --filtering_conditions '${filtering_conditions}'"
fi

if [[ ! -z $remove_recombination ]]; then
  full_cmd="$full_cmd --remove_recombination"
fi

if [[ ! -z $tree ]]; then
  full_cmd="$full_cmd --tree"
fi

if [[ ! -z $aws_batch_queue ]]; then
  full_cmd="$full_cmd --aws_batch_queue ${aws_batch_queue}"
fi

# Either debug or eval the final command
if [[ -z $debug ]]; then
  eval ${full_cmd}
else
  echo ${full_cmd}
fi

