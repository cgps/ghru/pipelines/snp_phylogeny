# Nextflow Workflow to generate a phylogeny based on mapping and SNP calling
This [Nextflow](https://www.nextflow.io/) workflow can be used to map multiple sample bacterial whole genome short read fastq file pairs to a reference genome and call variants which are then used to generate a phylogenetic tree.


## Running the workflow
Typically the workflow should be run as follows

```
nextflow run snp_phylogeny.nf [options] -resume -ansi 
```

This will run the standard profile which can be found in the [config file](nextflow.config) which specifies to run nextflow locally and in turn pulls in some standardiswed configuration from the [base config](base.config). The base configuration specifies that it will use docker and the container designed specifically for this workflow [bioinformant/ghru-snp-phylogeny](https://cloud.docker.com/u/bioinformant/repository/docker/bioinformant/ghru-snp-phylogeny)



The mandatory options that should be supplied are
  - A source of the fastq files specified as either of the following
    - `--input_dir` and `--fastq_pattern` arguments: from local files on disk
    - `--accession_number_file` argument: from a list of short read archive ERR or SRR accession numbers contained within a file specified by the 
  - `--output_dir` path to directory where the output from the pipeline will be written
  - `--reference_sequence` The reference genome (single chromosome only) should be specified as a path to a fasta sequence


Optional arguments include
  - `--adapter_sequences`: The path to a fasta file containing adapter sequences to trim from reads. If not specified it will use the [adapters.fas](adapters.fas) file from the nextflow workflow file directory
  - `--depth_cutoff`            The estimated depth to downsample each sample to. If not specified no downsampling will occur
  - `--tree`              Whether to create a tree. Set to fasttree, iqtree or auto.
                                Auto will try to use the --alignment_size_cutoff to determine the best tree algorithm to use
  - `--alignment_size_cutoff`   The number of total bases in the final alignment above which fasttree will be used to build the tree rather than iqtree. Default is 20 million bases
  - `--remove_recombination`      Whether to remove recombination from the combined alignment using gubbins before
                                producing the ML tree
  - `--filtering_conditions`    The VCF filtering conditions to use. The default is:
                                '%QUAL<25 || FORMAT/DP<10 || MAX(FORMAT/ADF)<5 || MAX(FORMAT/ADR)<5 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'
  - `--non_GATC_bases_threshold`The maximum fraction of non-GATC bases in a pseudogenome to accept. Those not passing will be written nto a file <OUTPUT DIR>/pseudogenomes/low_quality_pseudogenomes.tsv. Default is 0.5
  - `--termination_point`       The step in the pipeline to stop analysis. This must be one of bcfs, filtered_bcfs, pseudogenomes or pseudogenome_alignment.
                                If not specified the pipeline will continue through to the variant only alignment step and then on to tree building if --tree is set. 
  - `--simple_output`           Only output bcfs and tree file (if --tree specified). If an accession file was supplied the fastqs will also be published
  - `--full_output`             Output trimmed_fastqs and sorted bam files. These take up signficant disk space


A typical example of running the workflow would be as follows:
```
nextflow run snp_phylogeny/snp_phylogeny.nf \
--accession_number_file accessions.txt \
--reference reference.fna \
--output_dir snp_phylogeny_output \
--depth_cutoff 100 \
--remove_recombination \
--tree \
-resume -ansi -profile standard
```

If the input fastqs were local files then the `--accession_number_file` argument would be replaced by `--input-dir` with the path to the directory containing the pairs of fastq files and `fastq_pattern` containing the pattern to match pairs of fastqs such as `'*_{1,2}.fastq.gz'`

### Utility bash script
A simple utility script is provided to make this a little less verbose. To run the same workflow as above the script would be run as follows

```
snp_phylogeny/run_snp_phylogeny.sh \
-w snp_phylogeny \
-a accessions.txt \
-p . \
-r reference.fna \
-rr \
-t
```

`-p` specifies a parent directory where outputs will be written into a subdirectory called snp_phylogeny_output and temporary files into a subdirectory called work. If the the input files are not specified as accession numbers wih `-a` the fastq file pairs will be assumed to be in a subdirectory names `fastqs` in the parent directory and the fastq pairs will be assumed to match the pattern `'*{R,_}{1,2}*.fastq.gz'`. This can be changed if desired using the `-fq` argument.

If you like you can just append the `-d` which will not run the command but show the full nextflow command that would have been run without the debug `-d` argument





## Workflow process
The workflow consists of the following steps

1. (Optional) Fetch reads from the ENA
2. Trim reads using trimmomatic (dynamic MIN_LEN based on 30% of the read length)
3. Count number of reads and estimate genome size using Mash
4. Downsample reads if the `--depth_cutoff` argument was specified
5. Map reads to the specified reference genome with bwa mem
6. Call variants with samtools
7. Filter variants to flag low quality SNPs
8. Produce a pseudogenome based on the variants called. Missing positions are encoded as `-` characters and low quality positions as `N`
9. All pseudogenomes are concatenanted to make a whole genome alignment
10. (Optional) Recombination is removed from the alignment using gubbins
11. Invariant sites are removed using snp-sites
11. (Optional) Maximum likelihood tree generated using IQ-TREE


A sumary of this process is shown below in the diagram that was generated when running Nextflow using the -with-dag command

![workflow diagram](dag.png)

## Workflow outputs
These will be found in the directory specified by the `--output_dir` argument

  - (Optional) If accession numbers were used as the input source a directory called `fastqs` will contain the fastq file pairs for each accession number
  - A directory called `trimmed_fastqs` containing the reads after trimminb with TRIMMOMATIC
  - A directory called `sorted_bams` containing the alignmed sam files after mapping with bwa mem, conversion to bam and sorting
  - A directory called `filtered_bcfs` containing binary vcf files after filtering to flag low quality positions with LowQual in the FILTER column
  - A directory called `pseudogenomes` containing 
    - the pseudogenome from each sample
    - a whole genome alignment named `aligned_pseudogenome.fas` containing the concatenated sample pseudogenomes and the refrerence genome
    - a variant only alignment named `aligned_pseudogenome.variants_only.fas` with the invariant sites removed from `aligned_pseudogenome.fas` using snp-sites. If recombination removal was specified, the file will be named `aligned_pseudogenome.gubbins.variants_only.fas` with gubbins having been applied prior to invariant site removal.
  - Two newick tree files
   -  `aligned_pseudogenome.gubbins.variants_only.contree` If tree generation was specified, this file containing the consensus tree from IQ_TREE will be produced. The tree will possess assigned branch supports where branch lengths are optimized on the original alignment. If recombination removal was not specified the file will be named `aligned_pseudogenome.variants_only.contree`
   -  `aligned_pseudogenome.gubbins.variants_only.treefile` The original IQ-TREE maximum likelihood tree without branch supports. If recombination removal was not specified the file will be named `aligned_pseudogenome.variants_only.treefile`


## Software used within the workflow
  - [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) A flexible read trimming tool for Illumina NGS data.
  - [mash](https://mash.readthedocs.io/en/latest/) Fast genome and metagenome distance estimation using MinHash.
  - [seqtk](https://github.com/lh3/seqtk) A fast and lightweight tool for processing sequences in the FASTA or FASTQ format.
  - [bwa mem](https://github.com/lh3/bwa) Burrow-Wheeler Aligner for short-read alignment
  - [samtools](http://www.htslib.org/doc/samtools.html) Utilities for the Sequence Alignment/Map (SAM) format
  - [bcftools](http://www.htslib.org/doc/bcftools.html) Utilities for variant calling and manipulating VCFs and BCFs
  - [filtered_bcf_to_fasta.py](bin/filtered_bcf_to_fasta.py) Python utility to create a pseudogenome from a bcf file where each position in the reference genome is included
  - [gubbins](https://github.com/sanger-pathogens/gubbins/) Rapid phylogenetic analysis of large samples of recombinant bacterial whole genome sequences
  - [snp-sites](https://github.com/sanger-pathogens/snp-sites) Finds SNP sites from a multi-FASTA alignment file 
  - [IQ-TREE](http://www.iqtree.org) Efficient software for phylogenomic inference



