#!/usr/bin/env nextflow
/*

========================================================================================
                          GHRU SNP Calling Pipeline
========================================================================================
 Using primarily samtools/bcftools
----------------------------------------------------------------------------------------
*/

// Pipeline version
version = '1.2.2'

def versionMessage(){
  log.info"""
  ==============================================
   SNP Calling Pipeline version ${version}
  ==============================================
  """.stripIndent()
}
def helpMessage() {
    log.info"""
    Mandatory arguments:
      --reference_sequence  The path to a fasta file the reference sequence to which the reads will be mapped

    Alternate mandatory arguments:
    Either input_dir and fastq_pattern must be specified if using local short reads or accession_number_file if
      specifying samples in the SRA for which the fastqs will be downloaded

      --input_dir                 Path to input dir. This must be used in conjunction with fastq_pattern
      --fastq_pattern             The regular expression that will match fastq files e.g '*_{1,2}.fastq.gz'
      --accession_number_file     Path to a text file containing a list of accession numbers (1 per line)
      --bcfs                      Glob pattern to an input dir with unfiltered bcf files e.g 'input_dir/*.bcf'
      --filtered_bcfs             Glob pattern to an input dir with filtered bcf files e.g 'input_dir/*.filtered.bcf'
      --pseudogenomes             Glob pattern to an input dir with fasta pseudogenomes e.g 'input_dir/*.fas'
      --pseudogenome_alignment    Path to a pseudogenome alignment e.g input_dir/aligned_pseudogenomes.fas
      --output_dir                Path to output dir
      
    Optional arguments:
      --adapter_sequences         The path to a fasta file containing adapter sequences to trim from reads. If not specified
                                  those in the workflow basedir will be used
      --depth_cutoff              The estimated depth to downsample each sample to. If not specified no downsampling will occur
      --tree                Whether to create a tree. Set to fasttree, iqtree, nj or auto.
                                  Auto will try to use the --alignment_size_cutoff to determine the best tree algorithm to use
      --alignment_size_cutoff     The number of total bases in the final alignment above which fasttree will be used to build the tree rather than iqtree. Default is 20 million bases
      --remove_recombination      Whether to remove recombination from the combined alignment using gubbins before
                                  producing the ML tree
      --filtering_conditions      The VCF filtering conditions to use. The default is:
                                  '%QUAL<25 || FORMAT/DP<10 || MAX(FORMAT/ADF)<2 || MAX(FORMAT/ADR)<2 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'
      --non_GATC_bases_threshold  The maximum fraction of non-GATC bases in a pseudogenome to accept. Those not passing will be written
                                  into a file <OUTPUT DIR>/pseudogenomes/low_quality_pseudogenomes.tsv. Default is 0.5
      --termination_point         The step in the pipeline to stop analysis. This must be one of bcfs, filtered_bcfs, pseudogenomes or pseudogenome_alignment.
                                  If not specified the pipeline will continue through to the variant only alignment step and then on to tree building if --tree is set. 
      --simple_output               Only output bcfs and tree file (if --tree specified). If an accession file was supplied the fastqs will also be published
      --full_output               Output trimmed_fastqs and sorted bam files. These take up signficant disk space

   """.stripIndent()
}

//  print help if required
params.help = false
// Show help message
if (params.help){
    versionMessage()
    helpMessage()
    exit 0
}

// Show version number
params.version = false
if (params.version){
    versionMessage()
    exit 0
}
/***************** Setup inputs and channels ************************/
params.input_dir = false
params.accession_number_file = false
params.bcfs = false
params.filtered_bcfs = false
params.pseudogenomes = false
params.pseudogenome_alignment = false
params.fastq_pattern = false
params.adapter_file = false
params.output_dir = false
params.reference = false
params.depth_cutoff = false
params.tree = false
params.alignment_size_cutoff = false
params.remove_recombination = false
params.filtering_conditions = false
params.non_GATC_bases_threshold = false
params.simple_output = false
params.full_output = false
params.termination_point = false

// check if getting data either locally or from SRA
Helper.check_optional_parameters(params, ['input_dir', 'accession_number_file', 'bcfs', 'filtered_bcfs', 'pseudogenomes', 'pseudogenome_alignment'], [['input_dir', 'accession_number_file']])

//  check a pattern has been specified
if (params.input_dir){
  fastq_pattern = Helper.check_mandatory_parameter(params, 'fastq_pattern')
}
// set up output directory
output_dir = Helper.check_mandatory_parameter(params, 'output_dir') - ~/\/$/

if (params.accession_number_file || params.input_dir || params.bcfs || params.filtered_bcfs || params.pseudogenomes){
  // set up input from reference sequnce
  reference_sequence = file(Helper.check_mandatory_parameter(params, 'reference'))
}
// set up path to adapter sequences 
if ( params.adapter_file ) {
  adapter_file = params.adapter_file
} else {
  adapter_file = "${workflow.projectDir}/adapters.fas"
}
// assign depth cutoff
if ( params.depth_cutoff ) {
  depth_cutoff = params.depth_cutoff
} else {
  depth_cutoff = false
}

// assign filtering parameters
if ( params.filtering_conditions ) {
  filtering_conditions = params.filtering_conditions
} else {
  filtering_conditions = '%QUAL<25 || FORMAT/DP<10 || MAX(FORMAT/ADF)<2 || MAX(FORMAT/ADR)<2 || MAX(FORMAT/AD)/SUM(FORMAT/DP)<0.9 || MQ<30 || MQ0F>0.1'
}

// assign non GATC bases threshold
if ( params.non_GATC_bases_threshold ) {
  non_GATC_bases_threshold = params.non_GATC_bases_threshold
} else {
  non_GATC_bases_threshold = 0.5
}

// set termination point 
if (params.termination_point) {
  termination_point = Helper.check_parameter_value('termination_point', params.termination_point, ['bcfs', 'filtered_bcfs', 'pseudogenomes', 'pseudogenome_alignment'])
} else {
  termination_point = 'none'
}

// set simple_output 
if (params.simple_output) {
  simple_output = true
} else {
  simple_output = false
}


// set full_output 
if (params.full_output) {
  full_output = true
} else {
  full_output = false
}

// set build_tree
if (params.tree) {
  if (params.tree == 'fasttree'){
    tree = 'fasttree'
  } else if (params.tree == 'iqtree'){
    tree = 'iqtree'
  } else if (params.tree == 'nj'){
    tree = 'nj'
  } else if (params.tree == 'auto'){
    tree = 'auto'
  } else {
     tree = 'auto'
  }
} else {
  tree = false
}

if (params.alignment_size_cutoff){
  alignment_size_cutoff = params.alignment_size_cutoff
} else {
  alignment_size_cutoff = 20000000
}

//  ------------------ define pure Groovy methods ------------------ //
def find_genome_size(sample_id, mash_output) {
  m = mash_output =~ /Estimated genome size: (.+)/
  genome_size = Float.parseFloat(m[0][1]).toInteger()
  return [sample_id, genome_size]
}

def find_total_number_of_reads(sample_id, seqtk_fqchk_ouput){
  m = seqtk_fqchk_ouput =~ /ALL\s+(\d+)\s/
  total_reads = new BigDecimal(m[0][1]).toLong() * 2 // the *2 is an estimate since number of reads >q25 in R2 may not be the same
  return [sample_id, total_reads]
}
//  ------------------ end of pure Groovy methods ------------------ //

log.info "======================================================================"
log.info "                    SNP calling pipeline"
log.info "======================================================================"
log.info "SNP pipeline version   : ${version}"
if (params.accession_number_file){
  log.info "Accession File    : ${params.accession_number_file}"
} else if (params.input_dir){
  log.info "Fastq inputs      : ${params.input_dir}/${fastq_pattern}"
}
log.info "Reference                   : ${params.reference}"
log.info "======================================================================"
log.info "Outputs written to path     : ${params.output_dir}"
log.info "======================================================================"
log.info ""



if (params.accession_number_file || params.input_dir){
  // set up input from reference sequnce
  reference_sequence = file(Helper.check_mandatory_parameter(params, 'reference'))
  // index reference sequence
  process prepare_reference {
    tag {'prepare reference'}
    input:
    file ('reference.fasta') from reference_sequence

    output:
    file 'reference.*' into prepared_reference_files

    """
    bwa index reference.fasta
    """
  }

  if (params.accession_number_file){
    // Fetch samples from ENA
    accession_number_file = params.accession_number_file - ~/\/$/
    Channel
        .fromPath(accession_number_file)
        .splitText()
        .map{ x -> x.trim()}
        .set { accession_numbers }

    process fetch_from_ena {
      tag { sample_id }

      publishDir "${output_dir}/fastqs",
        mode: 'copy',
        saveAs: { file -> file.split('\\/')[-1] }

      input:
      val sample_id from accession_numbers

      output:
      set sample_id, file("${sample_id}/*.fastq.gz") into raw_fastqs_from_ena

      """
      enaDataGet -f fastq -as /aspera.ini ${sample_id}
      """
    }
  }
  if (params.input_dir) {
    input_dir = params.input_dir - ~/\/$/
    fastqs = input_dir + '/' + fastq_pattern
    Channel
      .fromFilePairs( fastqs )
      .ifEmpty { error "Cannot find any reads matching: ${fastqs}" }
      .set { raw_fastqs_from_local }
  }
  if (params.accession_number_file && params.input_dir){
    raw_fastqs_from_ena.concat(raw_fastqs_from_local).into {raw_fastqs_for_qc; raw_fastqs_for_trimming; raw_fastqs_for_length_assessment}
  } else {
    if (params.input_dir){
      raw_fastqs_from_local.into {raw_fastqs_for_qc; raw_fastqs_for_trimming; raw_fastqs_for_length_assessment}
    }
    if (params.accession_number_file){
      raw_fastqs_from_ena.into {raw_fastqs_for_qc; raw_fastqs_for_trimming; raw_fastqs_for_length_assessment}
    }
  }

  // Assess read length and make MIN LEN for trimmomatic 1/3 of this value
  process determine_min_read_length {
    tag { sample_id }

    input:
    set sample_id, file(file_pair) from raw_fastqs_for_length_assessment

    output:
    set sample_id, stdout into min_read_length

    """
    seqtk sample -s123 ${file_pair[0]} 1000 | printf "%.0f\n" \$(awk 'NR%4==2{sum+=length(\$0)}END{print sum/(NR/4)/3}')
    """
  }


  /*
  * trim reads
  */
  min_read_length_and_raw_fastqs = min_read_length.join(raw_fastqs_for_trimming)
  process trimming {
    tag { sample_id }
    
    if (full_output){
      publishDir "${output_dir}/trimmed_fastqs",
      mode: 'copy'
    }

    input:
    set sample_id, min_read_length, file(reads) from min_read_length_and_raw_fastqs
    file('adapter_file.fas') from adapter_file

    output:
    set sample_id, file("${sample_id}_1.trimmed.fastq.gz"), file("${sample_id}_2.trimmed.fastq.gz") into trimmed_fastqs_for_mapping,trimmed_fastqs_for_genome_size_estimation, trimmed_fastqs_for_read_counting

    """
    trimmomatic PE -threads 1 -phred33 ${reads} ${sample_id}_1.trimmed.fastq.gz ${sample_id}_1_unpaired.trimmed.fastq.gz ${sample_id}_2.trimmed.fastq.gz ${sample_id}_2_unpaired.trimmed.fastq.gz ILLUMINACLIP:adapter_file.fas:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:${min_read_length}
      """
  }

  // Genome Size Estimation
  process genome_size_estimation {
    tag { sample_id }

    input:
    set sample_id, file(for_reads), file(rev_reads)  from trimmed_fastqs_for_genome_size_estimation

    output:
    set sample_id, file('mash_stats.out') into mash_output

    """
    mash sketch -o /tmp/sketch_${sample_id}  -k 32 -m 3 -r ${for_reads}  2> mash_stats.out
    """
  }

  // channel to output genome size from mash output
  mash_output.map { sample_id, file -> find_genome_size(sample_id, file.text) } set {genome_size_estimation_for_downsampling}

  // Estimate total number of reads
  process count_number_of_reads {
    tag { sample_id }

    input:
    set sample_id, file(for_reads), file(rev_reads) from trimmed_fastqs_for_read_counting

    output:
    set sample_id, file('seqtk_fqchk.out') into seqtk_fqchk_output

    """
    seqtk fqchk -q 25 ${for_reads} > seqtk_fqchk.out
    """
  }

  read_counts = seqtk_fqchk_output.map { sample_id, file -> find_total_number_of_reads(sample_id, file.text) }

  fastqs_and_genome_size_and_read_count = trimmed_fastqs_for_mapping.join(genome_size_estimation_for_downsampling).join(read_counts).map{ tuple -> [tuple[0], tuple[1], tuple[2], tuple[3], tuple[4]]}

  // run snp pipeline to create filtered vcfs //
  // first map reads
  process map_reads {
    tag { sample_id }
    if (full_output){
      publishDir "${output_dir}/sorted_bams",
        mode: 'copy'
    }

    input:
    file ('reference.fasta') from reference_sequence
    file reference_file from prepared_reference_files
    set sample_id, file(for_reads), file(rev_reads), genome_size, read_count from fastqs_and_genome_size_and_read_count

    output:
    set sample_id, file("${sample_id}.sorted.bam") into sorted_bams

    script:
    if (depth_cutoff  && read_count/genome_size > depth_cutoff.toInteger()){
      downsampling_factor = depth_cutoff.toInteger()/(read_count/genome_size)
      """
      mkdir downsampled_fastqs
      seqtk sample  ${for_reads} ${downsampling_factor} | gzip > downsampled_fastqs/${for_reads}
      seqtk sample  ${rev_reads} ${downsampling_factor} | gzip > downsampled_fastqs/${rev_reads}
      bwa mem reference.fasta downsampled_fastqs/${for_reads} downsampled_fastqs/${rev_reads} | samtools view -bS -F 4 - | samtools sort -O bam -o ${sample_id}.sorted.possible_duplicates.bam -
      picard MarkDuplicates INPUT=${sample_id}.sorted.possible_duplicates.bam OUTPUT=${sample_id}.sorted.bam ASSUME_SORT_ORDER=coordinate METRICS_FILE=/dev/null VALIDATION_STRINGENCY=SILENT REMOVE_DUPLICATES=true
      """
    } else {
      """
      bwa mem reference.fasta ${for_reads} ${rev_reads} | samtools view -bS -F 4 - | samtools sort -O bam -o ${sample_id}.sorted.possible_duplicates.bam -
      picard MarkDuplicates INPUT=${sample_id}.sorted.possible_duplicates.bam OUTPUT=${sample_id}.sorted.bam ASSUME_SORT_ORDER=coordinate METRICS_FILE=/dev/null VALIDATION_STRINGENCY=SILENT REMOVE_DUPLICATES=true
      """
    }
  }

  // call variants
  process call_variants {
      tag { sample_id }
      if (simple_output || full_output || params.termination_point == 'bcfs'){
        publishDir "${output_dir}/bcfs",
        mode: 'copy'
      }

      input:
      file ('reference.fasta') from reference_sequence
      set sample_id, file(sorted_bam) from sorted_bams

      output:
      set sample_id, file("${sample_id}.bcf") into bcf_files

      script:
      """
      bcftools mpileup -a DP,AD,SP,ADF,ADR,INFO/AD,INFO/ADF,INFO/ADR -f reference.fasta ${sorted_bam} | bcftools call --ploidy 1 -m -Ob -o ${sample_id}.bcf
      """
  }
}
if (termination_point != 'bcfs'
    && (params.input_dir || params.accession_number_file || params.bcfs)
    )
  {
  if (params.bcfs) {
    bcf_files = Channel
                  .fromPath( params.bcfs )
                  .map { file -> tuple(file.baseName, file) }
  }
  // filter variants
  process filter_variants {
      tag { sample_id }
      if (! simple_output){
        publishDir "${output_dir}/filtered_bcfs",
          mode: 'copy'
      }

      input:
      set sample_id, file(bcf_file) from bcf_files

      output:
      set sample_id, file("${sample_id}.filtered.bcf") into filtered_bcf_files

      script:
      """
      bcftools filter -s LowQual -e'${filtering_conditions}' ${bcf_file} -Ob -o ${sample_id}.filtered.bcf

      """
  }
    }


if (termination_point != 'bcfs'
    && termination_point != 'filtered_bcfs'
    && (params.input_dir || params.accession_number_file || params.bcfs || params.filtered_bcfs)
    ){
  if (params.filtered_bcfs) {
    filtered_bcf_files = Channel
                          .fromPath( params.filtered_bcfs )
                          .map { file -> tuple(file.baseName, file) }
  }
  // produce pseudogenome
  process create_pseudogenome {
    tag { sample_id }
    if (! simple_output){
      publishDir "${output_dir}/pseudogenomes",
        mode: 'copy'
    }

    input:
    file ('reference.fasta') from reference_sequence
    set sample_id, file(filtered_bcf_file) from filtered_bcf_files

    output:
    file("${sample_id}.fas") into pseudogenomes

    script:
    """
    filtered_bcf_to_fasta.py  -r reference.fasta -b ${filtered_bcf_file} -o ${sample_id}.fas
    """
  }
}

if (termination_point != 'bcfs'
    && termination_point != 'filtered_bcfs'
    && termination_point != 'pseudogenomes'
    && (params.input_dir || params.accession_number_file || params.bcfs || params.filtered_bcfs || params.pseudogenomes)
    ){
  if (params.pseudogenomes) {
    pseudogenomes = Channel.fromPath( params.pseudogenomes )
  }
  process create_pseudogenome_alignment{
    publishDir "${output_dir}/pseudogenomes",
      mode: 'copy'

    input:
    file ('reference.fasta') from reference_sequence
    file(pseudogenomes) from pseudogenomes.collect { it }

    output:
    file('aligned_pseudogenome.fas') into aligned_pseudogenome
    file('low_quality_pseudogenomes.tsv')

    script:
    """
    touch low_quality_pseudogenomes.tsv
    for pseudogenome in ${pseudogenomes}
    do
      fraction_non_GATC_bases=`calculate_fraction_of_non_GATC_bases.py -f \$pseudogenome | tr -d '\\n'`
      if [[ 1 -eq "\$(echo "\$fraction_non_GATC_bases < ${non_GATC_bases_threshold}" | bc)" ]]; then
        cat \$pseudogenome >> aligned_pseudogenome.fas
      else
        echo "\$pseudogenome\t\$fraction_non_GATC_bases" >> low_quality_pseudogenomes.tsv
      fi
    done
    cat reference.fasta >> aligned_pseudogenome.fas
    """

  }
}

if (termination_point != 'bcfs'
    && termination_point != 'filtered_bcfs'
    && termination_point != 'pseudogenomes'
    && termination_point != 'pseudogenome_alignment'
    && (params.input_dir || params.accession_number_file || params.bcfs || params.filtered_bcfs || params.pseudogenomes || params.pseudogenome_alignment)
    ){
  if (params.pseudogenome_alignment) {
    aligned_pseudogenome = Channel.fromPath( params.pseudogenome_alignment )
  }
  process create_variant_only_alignment{
    memory '15GB'
    cpus 4
    publishDir "${output_dir}/pseudogenomes",
      mode: 'copy'

    input:
    file('aligned_pseudogenome') from aligned_pseudogenome

    output:
    file '*.fas' into aligned_pseudogenome_variants_only_for_iqtree,aligned_pseudogenome_variants_only_for_fasttree,aligned_pseudogenome_variants_only_for_njtree,aligned_pseudogenome_variants_only_for_size

    script:
    if (params.remove_recombination){
      """
      run_gubbins.py --threads 4 -v -t hybrid aligned_pseudogenome
      snp-sites aligned_pseudogenome.filtered_polymorphic_sites.fasta -o aligned_pseudogenome.gubbins.variants_only.fas
      """

    } else {
      """
      snp-sites aligned_pseudogenome -o aligned_pseudogenome.variants_only.fas
      """
    }


  }
}

if (termination_point != 'bcfs'
    && termination_point != 'filtered_bcfs'
    && termination_point != 'pseudogenomes'
    && termination_point != 'pseudogenome_alignment'
    && tree){
  process get_alignment_length{
    tag {'get alignment_size'}

    input:
    val alignment_size_cutoff
    file('alignment.fas') from aligned_pseudogenome_variants_only_for_size

    output:
    stdout into (auto_tree,auto_tree_for_iqtree,auto_tree_for_fasttree,auto_tree_for_njtree)

    when:
    tree == 'auto'

    script:
    """
    NUM_SEQS=\$(calculate_alignment_size.py -a alignment.fas | awk '{print \$1}')
    LEN_ALIGN=\$(calculate_alignment_size.py -a alignment.fas | awk '{print \$2}')
    TOTAL_BASES=\$(( \$NUM_SEQS * \$LEN_ALIGN ))
    if [[ \$TOTAL_BASES -gt $alignment_size_cutoff ]]
    then
      echo -n 'fasttree'
    else
      echo -n 'iqtree'
    fi
    """
  }

  process no_autotree {
    output:
    stdout into no_auto_tree

    when:
    tree != 'auto'

    script:
    """
    echo -n 'no auto tree'
    """
  }


  process build_iqtree {
    memory { 15.GB * task.attempt }
    cpus 4


    publishDir "${output_dir}",
      mode: 'copy'

    input:
    val(auto_tree_value) from auto_tree_for_iqtree.mix(no_auto_tree)
    file('aligned_pseudogenome.variants_only') from aligned_pseudogenome_variants_only_for_iqtree

    output:
    file("*.treefile")
    file("*.contree")

    when:
    tree == 'iqtree' || auto_tree_value == 'iqtree'

    script:
    if (params.remove_recombination){
      """
      iqtree -s aligned_pseudogenome.variants_only -pre aligned_pseudogenome.gubbins.variants_only -m GTR+G -alrt 1000 -bb 1000 -nm 200 -nt AUTO -ntmax 4
      """
    } else {
      """
      iqtree -s aligned_pseudogenome.variants_only -m GTR+G -alrt 1000 -bb 1000 -nm 200 -nt AUTO -ntmax 4
      """
    }
  }
  process build_fasttree {
    memory { 8.GB * task.attempt }
    publishDir "${output_dir}",
      mode: 'copy'

    input:
    val(auto_tree_value) from auto_tree_for_fasttree.mix(no_auto_tree)
    file('aligned_pseudogenome.variants_only') from aligned_pseudogenome_variants_only_for_fasttree

    output:
    file("*.tre")

    when:
    tree == 'fasttree'|| auto_tree_value == 'fasttree'

    script:
    if (params.remove_recombination){
      """
      fasttree -nt -gtr aligned_pseudogenome.variants_only > aligned_pseudogenome.gubbins.variants_only.tre
      """
    } else {
      """
      fasttree -nt -gtr aligned_pseudogenome.variants_only > aligned_pseudogenome.variants_only.tre
      """
    }
  }
  process build_njtree {
    memory { 4.GB * task.attempt }
    publishDir "${output_dir}",
      mode: 'copy'

    input:
    val(auto_tree_value) from auto_tree_for_njtree.mix(no_auto_tree)
    file('aligned_pseudogenome.variants_only') from aligned_pseudogenome_variants_only_for_njtree

    output:
    file("*.tre")

    when:
    tree == 'nj'|| auto_tree_value == 'nj'

    script:
    if (params.remove_recombination){
      """
      python3.6 -c 'from Bio import SeqIO; SeqIO.convert("aligned_pseudogenome.variants_only", "fasta", "aligned_pseudogenome.variants_only.sth", "stockholm")'
      rapidnj aligned_pseudogenome.variants_only.sth -i sth -t d -b 1000 -n -c 1 -x aligned_pseudogenome.gubbins.variants_only.tre
      """
    } else {
      """
      python3.6 -c 'from Bio import SeqIO; SeqIO.convert("aligned_pseudogenome.variants_only", "fasta", "aligned_pseudogenome.variants_only.sth", "stockholm")'
      rapidnj aligned_pseudogenome.variants_only.sth -i sth -t d -b 1000 -n -c 1 -x aligned_pseudogenome.variants_only.tre
      """
    }
  }
}


workflow.onComplete {
  Helper.complete_message(params, workflow, version)
}

workflow.onError {
  Helper.error_message(workflow)
}
